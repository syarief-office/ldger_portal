/* Author:http://www.rainatspace.com

*/

function equalHeight(group) {
		tallest = 0;
		jQuery(window).on("load resize", function(){
			if (jQuery(window).width() > 979  ) {
				group.each(function() {
					thisHeight = jQuery(this).innerHeight();
					if(thisHeight > tallest) {
						tallest = thisHeight;
					}
				});
				group.height(tallest);
			}
		});
}

function initializeScript(){
	//jQuery('#preloader').fadeOut(2000);
		jQuery("[icon-data]").each(function(){
		var getAttr =  jQuery(this).attr('icon-data');
		jQuery(this).addClass(getAttr).removeAttr('icon-data');
	});

	// NAVIGATION HANDLER
	jQuery(".navigation > ul").clone(false).find("ul,li").removeAttr("id").remove(".submenu").appendTo(".nav-rwd-sidebar > ul");
	jQuery(".btn-rwd-sidebar, .btn-hide").click( function() {
		jQuery(".nav-rwd-sidebar").toggleClass("sidebar-active");
		jQuery(".wrapper-inner").toggleClass("wrapper-active");
	});

	jQuery('.btn-src').on('click', function(e) {
		e.preventDefault();
		jQuery('#search').animate({width: 'toggle'}).focus();
	});

	jQuery('.criteria-item').on("click",function(){
		jQuery('.criteria-item').removeClass('active');
		jQuery(this).addClass('active');

		return false;
	});

	setInterval(function() {
      jQuery(".bounce").effect( "bounce", 
          {times:4}, 2500 );
     },4000);

	//ASIDE SLIDE
	jQuery(".btnSlide").on('click', function(){
		//$('.asideSlide').css('margin-right', '-385px');
		jQuery('.asideSlide').toggleClass('hideBox', function(){
			if(jQuery(this).is('.hideBox')){
				jQuery(this).find('.btnSlide').removeClass('active');
			}else{
				jQuery(".btnSlide").addClass('active');
			}
		})
		return false;
	});

	equalHeight(jQuery(".eqHeight"));


	//ADD INPUT FIELD
	jQuery('.addRow').on('click', function(){
		event.preventDefault();
		jQuery('.scPool-row').append(
			'<div class="scPool">\
				 <form>	\
					<div class="inputbox_2">\
						<select class="form-control">\
						  <option>1</option>\
						  <option>2</option>\
						  <option>3</option>\
						  <option>4</option>\
						  <option>5</option>\
						</select>\
					</div>\
					<div class="inputbox_2">\
						<input type="text" class="form-control fr-border">\
					</div>\
					<a href="#" class="btn-minus removeRow"><span>-</span></a>\
				</form>\
			</div> <!-- //scPool -->'
		);
	});
	//Remove INPUT FIELD
	jQuery("body").on("click", ".removeRow", function (e) {
	 event.preventDefault();
	 jQuery(this).closest('.scPool').remove();
	});

	//LOGIN WRAP BODY
	jQuery(window).load(function(){
		jQuery('.login-section').css({
			'height' : jQuery(window).innerHeight()
		});
	});

	//MODAL
	var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");
	jQuery('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    	jQuery("body").append(appendthis);
    	jQuery(".modal-overlay").fadeTo(500, 0.7);
    	//$(".js-modalbox").fadeIn(500);
		var modalBox = jQuery(this).attr('data-modal-id');
		jQuery('#'+modalBox).fadeIn($(this).data());
	});  
	jQuery(".js-modal-close, .modal-overlay").click(function() {
	    jQuery(".modal-box, .modal-overlay").fadeOut(500, function() {
	        jQuery(".modal-overlay").remove();
	    });
	 
	});
	jQuery(window).load(function() {
	    jQuery(".modal-box").css({
	        top: (jQuery(window).height() - jQuery(".modal-box").outerHeight()) / 2,
	        left: (jQuery(window).width() - jQuery(".modal-box").outerWidth()) / 2
	        
	    });
	    jQuery(".modal-box .modal-body").css({
	    	height : jQuery(window).height() - 100
	    })
	});
	jQuery(window).load();
}

/* =Document Ready Trigger
-------------------------------------------------------------- */
jQuery(document).ready(function(){
    initializeScript();
});
/* END ------------------------------------------------------- */